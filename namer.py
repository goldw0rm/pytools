# -*- coding: cp949 -*-

import os

def renameAll(title, ext):
    """
    ext contains dot character.
    """
    files = getFilesByExt(ext)

    # the list of files of which names contain the file extension
    files.sort()
    numOfFiles = len(files)
    fileNameFormat = getFileNameFormat(numOfFiles)

    for i in range(numOfFiles):
        filename = getNewFileName(fileNameFormat, title, i+1, ext)

        file = files[i]
        print(file + " --> " + filename)
        os.rename(file, filename)

def getFilesByExt(ext):
    fileList = os.listdir('.')
    files = []

    for file in fileList:
        if containsExtension(file, ext):
            files.append(file)

    return files

def detectFileExtensions():
    '''
    Extracts two extensions whose numbers are larger than others.
    Ex) [ 'smi', 'mp4' ]
    '''
    fileList = os.listdir('.')
    exts = {}

    for filename in fileList:
        name, ext = os.path.splitext(filename)
        if len(ext) > 1 and ext.startswith('.'):
            ext = ext[1:].lower()
            exts[ext] = exts.get(ext, 0) + 1

    print(exts)
    if len(exts) < 2:
        return

    sortedExts = sorted(exts.items(), key=lambda x: x[1], reverse=True)
    if sortedExts[0][1] == sortedExts[1][1]:
        if len(sortedExts) == 2 or sortedExts[1][1] > sortedExts[2][1]:
            return [ sortedExts[0][0], sortedExts[1][0] ]

def getFileNameFormat(numOfFiles):
    """
    Returns filename format
    """
    digits = 0
    while numOfFiles > 0:
        numOfFiles //= 10
        digits += 1

    digits = max(2, digits)
    return '%s %0{0}d%s'.format(digits)

def getNewFileName(fileNameFormat, title, i, ext):
    """
    ext contains dot character.
    """
    # ex) title 01.mp4
    return fileNameFormat % (title, i, ext)

def containsExtension(filename, inputExt):
    """
    ext contains dot character.
    """
    base, ext = os.path.splitext(filename)
    return ext.lower() == inputExt.lower()

def getParentPath():
    """
    If title is empty, then use parent's directory name as a default title.
    """
    return os.path.basename(os.getcwd())

def main():
    extensions = []
    title = input('Input file title: ').strip()
    if title == '':
        title = getParentPath()
        print('Title: {0}'.format(title))

    for i in range(1, 11):
        ext = input('Input file extension {0}: '.format(i))
        if len(ext) == 0:
            break

        extensions.append(ext)

    if len(extensions) == 0:
        extensions = detectFileExtensions()
        if extensions is None:
            print('Failed to detect FileExtensions.')
            return 1
        else:
            print('Extensions: {0}, {1}'.format(extensions[0], extensions[1]))

    for ext in extensions:
        print('---------- {0} ----------'.format(ext))
        renameAll(title, '.'+ext)

if __name__ == '__main__':
    from sys import version_info
    if version_info.major < 3:
        print('python 2.x is not supported.')
        exit(1)

    main()
